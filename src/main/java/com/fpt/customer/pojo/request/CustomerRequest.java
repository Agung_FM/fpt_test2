package com.fpt.customer.pojo.request;


import com.sun.istack.NotNull;
import org.springframework.lang.NonNullFields;

public class CustomerRequest {



    private Integer id;

    private String name;

    private String address;

    public Integer getId() {
        if (this.id==null){
            this.id=0;
        }
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        if (this.name==null){
            this.name="";
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        if (this.address==null){
            this.address="";
        }
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public CustomerRequest() {
    }

    public CustomerRequest(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public CustomerRequest(Integer id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }
}
