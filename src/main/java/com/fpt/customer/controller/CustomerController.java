package com.fpt.customer.controller;

import com.fpt.customer.entity.Customer;
import com.fpt.customer.pojo.request.CustomerRequest;
import com.fpt.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("AllCustomer")
    public List<Customer> getCustomerAll(){
        return customerService.getAll();
    }

    @GetMapping("CustomerByIdOrName")
    public Customer getCustomerByIdOrName(@RequestBody CustomerRequest customer){
        if (!customer.getName().equals("")){
            return customerService.getByName(customer.getName());
        }else{
            return customerService.getById(customer.getId());
        }
    }

    @PostMapping("SaveCustomer")
    public String saveCustomer(@RequestBody CustomerRequest customer){
        if (!customer.getName().equals("")&&!customer.getAddress().equals("")){
            customerService.saveCustomer(customer);
            return "Customer Data Saved";
        }else{
            return "Name or Address cannot be empty";
        }
    }

    @PostMapping("DeleteCustomerByIdOrName")
    public String deleteCustomerByIdOrName(@RequestBody CustomerRequest customer){
        if (!customer.getName().equals("")){
            return customerService.deleteByName(customer.getName());
        }else if(!customer.getId().equals("")){
            return customerService.deleteById(customer.getId());
        }else{
            return "ID or Name cannot be empty";
        }
    }


    @PostMapping("UpdateCustomer")
    public String updateCustomerById(@RequestBody CustomerRequest customer){
        if (customer.getId().equals("")){
            return "ID cannot be empty";
        }else if(!customer.getName().equals("")&&!customer.getAddress().equals("")){
            return customerService.updateCustomerById(customer);
        }else{
            return "Name and Address cannot be empty";
        }
    }
}
