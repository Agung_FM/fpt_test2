package com.fpt.customer.dao;

import com.fpt.customer.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerDao extends JpaRepository<Customer, Integer> {

    public Customer getByNameLike(String name);
}
