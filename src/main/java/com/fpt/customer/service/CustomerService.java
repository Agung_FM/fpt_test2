package com.fpt.customer.service;

import com.fpt.customer.entity.Customer;
import com.fpt.customer.pojo.request.CustomerRequest;

import java.util.List;

public interface CustomerService {

    public List<Customer> getAll();

    public Customer getById(Integer Id);

    public Customer getByName(String name);

    public Customer saveCustomer(CustomerRequest customer);

    public String deleteById(Integer Id);

    public String deleteByName(String name);

    public String updateCustomerById(CustomerRequest customer);
}
