package com.fpt.customer.service.impl;

import com.fpt.customer.dao.CustomerDao;
import com.fpt.customer.entity.Customer;
import com.fpt.customer.pojo.request.CustomerRequest;
import com.fpt.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;


    @Override
    public List<Customer> getAll() {
        return customerDao.findAll();
    }

    @Override
    public Customer getById(Integer Id) {
    return customerDao.getOne(Id);
    }

    @Override
    public Customer getByName(String name) {
        return customerDao.getByNameLike(name);
    }

    @Override
    public Customer saveCustomer(CustomerRequest customer) {

        return customerDao.save(new Customer(customer.getName(),customer.getAddress()));
    }

    @Override
    public String deleteById(Integer Id) {
        Customer customer = customerDao.getOne(Id);
        if (customer!=null){
            customerDao.delete(customer);
            return "Delete User : "+Id+"success";
        }else{
            return "Delete User : "+Id+"failed";
        }
    }

    @Override
    public String deleteByName(String name) {
        Customer customer = customerDao.getByNameLike(name);
        if (customer!=null){
            customerDao.delete(customer);
            return "Delete User : "+name+"success";
        }else{
            return "Delete User : "+name+"failed";
        }

    }

    @Override
    public String updateCustomerById(CustomerRequest customer) {
        Customer customerDB = customerDao.getOne(customer.getId());
        if (customerDB!=null){
            customerDB.setName(customer.getName());
            customerDB.setAddress(customer.getAddress());
            customerDao.save(customerDB);
            return "Update User : "+customer.getName()+"success";
        }else{
            return "Update User : "+customer.getName()+"failed";
        }
    }
}
